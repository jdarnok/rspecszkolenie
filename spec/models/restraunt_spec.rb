
describe Restraunt do
  context 'with valid data' do
    before do
      @restraunt = build(:restraunt)
    end
    it 'has a valid factory' do
      expect(@restraunt).to be_valid
    end
    it 'has valid name' do
      restaurant = build_stubbed(:restraunt)
      expect(restaurant.errors[:name].size).to eq(0)
    end
    it 'has valid rating' do
      restaurant = build_stubbed(:restraunt)
      expect(restaurant.errors[:rating].size).to eq(0)
    end
    it 'has valid address' do
      restaurant = build_stubbed(:restraunt)
      expect(restaurant.errors[:address].size).to eq(0)
    end
  end
  context 'with invalid data' do
    before do
      @restraunt = build_stubbed(:restraunt)
    end
    it 'has valid factory' do
      expect(@restraunt).to be_valid
    end
    it 'has invalid name' do
      restraunt = build(:restraunt, name: nil)
      restraunt.save
      expect(restraunt.errors[:name].size).to eq(1)
    end
  end
end
