describe Category do
  before do
    @category = build_stubbed(:category)
  end
  it 'has valid factory' do
    expect(@category).to be_valid
  end

  it 'is invalid without a name' do
    category = build(:category, name: nil)
    category.save
    expect(category.errors[:name].size).to eq(1)
  end
  it 'has unique name' do
    first_category = create(:category, name: 'foo')
    category = build(:category, name: 'foo')
    category.save
    expect(category.errors[:name].size).to eq(1)
  end
end
