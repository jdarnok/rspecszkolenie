describe Dish do
  before do
    @dish = build_stubbed(:dish)
  end
  it 'has a valid factory' do
    expect(@dish).to be_valid
  end
  it 'has category' do
    expect {@dish.category}.to_not raise_error
  end
  it 'has restraunt' do
    expect {@dish.restraunt}.to_not raise_error
  end
 end
