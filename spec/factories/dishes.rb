FactoryGirl.define do
  factory :dish do
    association :restraunt, factory: :restraunt
    association :category, factory: :category
    name { Faker::Company.name }
    price { rand(0.0..100.00) }
  end
end
