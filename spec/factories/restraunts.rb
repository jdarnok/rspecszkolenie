FactoryGirl.define do
  factory :restraunt do
    name { Faker::Company.name }
    address { Faker::Address.street_name }
    rating { rand(0..5) }
  end
end
