class CreateDishes < ActiveRecord::Migration
  def change
    create_table :dishes do |t|
      t.references :restraunt, index: true, foreign_key: true
      t.references :category, index: true, foreign_key: true
      t.string :name
      t.float :price

      t.timestamps null: false
    end
  end
end
